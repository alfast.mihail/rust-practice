use  std::str::{FromStr}; 

fn main(){
	let mut gas_tank = input::<f32>("How much does the vehicle's gas tank hold?\n");
	let mut gas_consume = 100.0/ input::<f32>("How many liters of gas does the car consume per hundred kilometers?\n");
	let mut prev_coordinates = Point { x_axis: 0.0, y_axis: 0.0};
	let mut new_coordinates = Point { x_axis: 0.0, y_axis: 0.0};
	menu(&mut new_coordinates, &mut prev_coordinates, &mut gas_tank, &mut gas_consume)
}

fn menu(new_coordinates: &mut Point, prev_coordinates: &mut Point, gas_tank: &mut f32, gas_consume: &mut f32){
	println!("Coordinates x={}, y={}, the tank contains {} liters of gas.", new_coordinates.x_axis, new_coordinates.y_axis, gas_tank);
	let option = input::<i64>("1) Fill 2) Drive 3) Quit\n");
	if option == 1{
		fill(gas_tank);
		menu(new_coordinates, prev_coordinates, gas_tank, gas_consume)
	} else if option == 2{
		drive(new_coordinates, prev_coordinates, gas_tank, gas_consume);
		menu(new_coordinates, prev_coordinates, gas_tank, gas_consume)
	} else if option == 3{
		println!("Thank you and bye!")
	} else {
		println!("Wrong option");
		menu(new_coordinates, prev_coordinates, gas_tank, gas_consume)
	}
}

fn fill(gas_tank: &mut f32) -> &mut f32{
	*gas_tank += input::<f32>("How many liters of gas to fill up?\n");
	gas_tank
} 

fn drive(new_coordinates: &mut Point, prev_coordinates: &mut Point, gas_tank: &mut f32, gas_consume: &mut f32){
	new_coordinates.x_axis = input::<f32>("x:");
	new_coordinates.y_axis = input::<f32>("y:");
	let hypotenuse = ((new_coordinates.x_axis - prev_coordinates.x_axis).powi(2) + (new_coordinates.y_axis - prev_coordinates.y_axis).powi(2)).sqrt();
	if *gas_tank * *gas_consume - hypotenuse < 0.0{
		let sin = new_coordinates.y_axis / hypotenuse ;
		new_coordinates.y_axis = sin * ( *gas_consume * *gas_tank);
		new_coordinates.x_axis = ((*gas_consume * *gas_tank).powi(2) - new_coordinates.y_axis.powi(2)).sqrt();
		*gas_tank = 0.0;
		} else {
		*gas_tank = *gas_tank - hypotenuse/ *gas_consume
		}
	prev_coordinates.x_axis = new_coordinates.x_axis;
	prev_coordinates.y_axis = new_coordinates.y_axis;
	}
struct Point {
	x_axis: f32,
	y_axis: f32,
}

fn input<F>(prompt: &str) -> F where
   F: FromStr {
      use std::io::{stdin, self, Write};
      print!("{}", prompt);
      io::stdout().flush().unwrap(); 
      let mut guess = String::new();
      let _= stdin().read_line(&mut guess);
      let a = guess.to_string().trim().parse::<F>();
      match a {
            Ok(v) => v,
            Err(_) => panic!("Parse failed!")
      }
}
